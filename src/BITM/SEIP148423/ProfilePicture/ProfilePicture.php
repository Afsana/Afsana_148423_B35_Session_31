<?php

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB{

    public $id="";
    public $name="";
    public $image="";

    public function __construct()
    {
        parent::__construct();
    }

/*
    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->profile_picture."<br>";
    }
*/

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('image',$data)){
            $this->image=$data['image']['name'];
        }
        //var_dump($this->image);die;

    }

    public function store(){

        $path='C:\xampp\htdocs\Afsana_148423_B35_Lab_Exam_8\resource\assets\img\backgrounds/';
        $uploadedFile = $path.basename($this->image);
        move_uploaded_file($_FILES['image']['tmp_name'], $uploadedFile);
        $arrData=array($this->name, $this->image);
        $sql="INSERT INTO  profile_picture(name,image) VALUES (?,?)";

        //var_dump($sql);die;
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method


    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from profile_picture where is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from profile_picture where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){

        $path='C:\xampp\htdocs\Afsana_148423_B35_Lab_Exam_8\resource\assets\img\backgrounds/';
        //$arrData = array ($this->name, $this->image);
        //var_dump($arrData);die;
        //$sql = "UPDATE profile_picture SET name = ?, image = ? WHERE id =".$this->id;

        if(!empty($_FILES['image']['name'])) {
            $uploadedFile = $path.basename($this->image);
            //var_dump($uploadedFile);die;
            move_uploaded_file($_FILES['image']['tmp_name'],$uploadedFile);
            $arrData = array( $this->name,$this->image);
            $sql="UPDATE profile_picture SET name = ?, image = ? WHERE id=".$this->id;
            //var_dump($sql);die;
        }
        else{
            $arrData = array( $this->name);
            $sql="UPDATE profile_picture SET name = ? WHERE id=".$this->id;
        }

//var_dump($sql);die;
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from profile_picture where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update profile_picture SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from profile_picture where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();


    public function recover(){

        $sql = "Update profile_picture SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();

    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();



    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byImage']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `image` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byImage']) ) $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byImage']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='No' AND `image` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        //$WordsArr = array();
        $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->image);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->image);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



}//end of ProfilePicture();


//$objProfilePicture = new ProfilePicture();

?>
